package Exam;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class App extends JFrame {

    JTextField jTextField1;
    JButton jButton;

    App() {
        setTitle("My application");
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        init();
        setSize(300, 300);
        setVisible(true);
    }

    public void init() {
        setLayout(null);
        jTextField1 = new JTextField();
        jTextField1.setBounds(10, 20, 200, 20);
        jButton = new JButton("Press");
        jButton.setBounds(10, 200, 200, 20);

        add(jTextField1);
        add(jButton);

        jButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String input = jTextField1.getText();
                StringBuilder result = new StringBuilder(input);
                result.reverse();
                //jTextField1.setText(result.toString());
                System.out.println(result.toString());
            }
        });

    }
    public static void main(String[] args) {
        new App();
    }
}